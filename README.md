### PowerShell V4 Notes



 - Using the WSMan PowerShell provider on 2012R2-MS, collect the current value for each of the following remoting configuration properties: 

```

cd wsman:
ls .\localhost\Shell\IdleTimeout
ls .\localhost\Shell\AllowRemoteShellAccess
ls .\localhost\Shell\MaxShellsPerUser
ls .\localhost\Service\AllowRemoteAccess
ls .\localhost\Service\IPv4Filter
ls .\localhost\Service\AllowUnencrypted
ls .\localhost\Service\DefaultPorts\HTTP
```


 - What is the Windows PowerShell command to enable PowerShell remoting for a Public network on a client Operating System? 

```
Enable-PSRemoting -SkipNetworkProfileCheck 
```

 - Debug a executable

```
 Trace-Command -PSHost -Expression {Start-Process notepad} -Option all -name ParameterBinding
```
 
 