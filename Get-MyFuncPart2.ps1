Function get-MyFuncPart2
{
<#
.Synopsis
   Working in progress
.DESCRIPTION
   This function will be a Jedi someday.
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   get-MyFuncPart2 -svc XblGameSave  -comp localhost
.EXAMPLE
   get-MyFuncPart2 -svc server  -comp localhost
.EXAMPLE
   Kill-Process -Name notepad
.INPUTS
   NONE
.OUTPUTS
   Service
.LINK
    https://gitlab.com/marloncabrera/PowerShellv4
.NOTES
   This function was created on 03/15/2017
.COMPONENT
   Services and etc...
.ROLE
   Dev Ops
.FUNCTIONALITY
   Learn PowerShell V4
#>

    [cmdletbinding()]
    param ([parameter(position=1, mandatory = $true, HelpMessage = "Please help me")][string]$svc,[parameter(position=0)][string[]]$comp = "localhost", [switch]$force)
    Get-Service -Name $svc -RequiredServices -ComputerName $comp
    Write-Verbose "Noisy comment"
    Write-Debug "Now debugging"

    if ($force) {Stop-Service $svc} else {""}
}

Function Kill-Process
{
    [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='Medium')]

    Param([String]$Name)
    $TargetProcess = Get-Process -Name $Name
    If ($PSCmdlet.ShouldProcess($name, "Terminating Process"))
    {
        $TargetProcess.Kill()
    }
}